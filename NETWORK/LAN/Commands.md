---
Author: "lucky777"

Header:
    Left: ""
    Center: ""
    Right: ""
Footer:
    Left: "{{CurrentDate}}"
    Center: <span class="pageNumber"></span>/<span class="totalPages"></span>
    Right: "by {{Author}}"
---

# Linux newtorking commands

```sh
"[TODO]" ssh $x -p $x
"[TODO]" ps aux | grep ssh
"[TODO]" systemctl ssh start/stop
"[TODO]" systemctl ssh enable/disable
"[TODO]" iptables -nvL
```

- [nslookup](#nslookup)
- [tcpdump](#tcpdump)
- [ethtool](#ethtool)
- [ip](#ip)
  * [ip address](#ip-address)
    + [show](#show)
    + [add](#add)
    + [del](#del)
  * [ip link](#ip-link)
    + [show](#show-1)
    + [set](#set)
  * [ip route](#ip-route)
    + [show](#show-2)
    + [add](#add-1)
    + [del](#del-1)
  * [ip neighbour show](#ip-neighbour-show)
- [arp](#arp)
  * [arp-scan](#arp-scan)

---

# nslookup

    Gives informations about a web link

`nslookup example.com`

```sh
#usage:
nslookup $link
#example:
nslookup protonmail.com
"Server:         127.0.0.53
Address:        127.0.0.53#53

Non-authoritative answer:
Name:   protonmail.com
Address: 185.70.42.1"
```

# tcpdump

    Network sniffer (spying on traffic) similar to WireShark

`tcpdump -eni wlo1`

```sh
#useful arguments:
-e    #shows MAC addresses
-n    #I don't know and I'm too lazy to search for it
-i    #specify interface
#example:
tcpdump -eni wlo1 #scans traffic on wireless interface 'wlo1'
```

# ethtool

    Interface configuration

`ethtool enp3s0`

```sh
#notation:
ethtool $interface
#arguments:
just write 'ethtool --help'
there are a lot of them and I feel tired right now blbl
#example:
ethtool enp3s0
```

# ip

    Configuration tools for network interfaces

## ip address

> Configuration by IP addresse

### show

Shows network configuration by ip

`ip address show`

```sh
#notations:
ip address show dev $interface #dev = device
ip addr show
ip addr s
ip a s
ip a
#useful arguments:
--oneline  -o #unique lines
--brief   -br #less informations
--color    -c #with colors
--inet     -4 #ipv4 addresses
--inet6    -6 #ipv6 addresses
#examples:
ip --color --brief addr show #colored brief
ip -c -br a   #colored brief
ip -o -c -6 a #oneline colored ipv6
ip a dev eth0 #only interface eth0
#available for link and route:
ip link show  #shows by interfaces
ip route show #shows routes
```

### add

Adds an ip address to an interface

`ip address add x.x.x.x dev x`

```sh
#notations:
ip addr add $x.$x.$x.$x dev $x
ip a add $x.$x.$x.$x dev $x
#example:
ip addr add 192.168.0.77 dev enp3s0 #wired interface
```

### del

Deletes an ip address of an interface

`ip address del x.x.x.x dev x`

```sh
#notations:
ip addr del $x.$x.$x.$x dev $x
ip a del $x.$x.$x.$x dev $x
#example:
ip addr del 192.168.0.42 dev wlo1 #wireless interface
```

## ip link

> Configuration by interface

### show

Shows network configuration by interface

`ip link show`

```sh
#notations:
ip link show
ip link s
ip l s
ip l
```

### set

Enables and disables an interface

`ip link set x up`

```sh
#options:
ip link set $interface up   #enables
ip link set $interface down #disables
#examples:
ip link set enp3s0 up #enables wired interface 'enp3s0'
ip l set wlo1 down    #disables wifi interface 'wlo1'
```

## ip route

> Routes configuration

### show

Shows routes configuration

`ip route show`

```sh
#other notations:
ip route s
ip r s
ip r
```

### add

Adds a route

`ip route add x.x.x.x via x.x.x.x`

```sh
#examples:
ip route add 192.168.0.77 via 192.168.0.1
ip route add default via 192.168.0.1 #adds default gateway
```

### del

Deletes a route

`ip route del x.x.x.x via x.x.x.x`

```sh
#examples:
ip route del 192.168.0.77 via 192.168.0.1
ip route del default via 192.168.0.1 #deletes default gateway
```

## ip neighbour show

> Shows all devices on the same network we are connected to
>
>> `We won't see the devices we never communicated with!` If you want to detect all devices, use `arp-scan`

```sh
#notations:
ip neighbour s
ip neigh s
ip n s
ip n
```

# arp

    address resolution protocol

```sh
#useful arguments:
--set     -s #new entry
--delete  -d #deletes entry
--device  -i #chooses interface
--numeric -n #shows only ip addresses, no name
#examples:
arp --numeric #shows infos with ip addresses
arp --device enp3s0 --set 192.168.0.42 #adds
arp -device wlo1 -s 192.168.0.77       #adds
arp -i enp3s0 -d 192.168.0.42          #deletes
```

## arp-scan

    Scans all devices connected on the network

```sh
#installation:
sudo apt install arp-scan
#useful arguments:
--localnet   -l #scans on local network
--interface  -I #scans on a specified interface (must support arp)
--numeric    -N #shows only ip addresses, no name
#examples:
sudo arp-scan --localnet #scans on local network
sudo arp-scan -I wlo1 192.168.0.1/24 #interface 'wlo1' network by ip
```

```java
public class Test {
  public static void main(String[] args) {
    System.out.println("lucky world!");
  }
}
```